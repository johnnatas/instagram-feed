// Importaçôes das dependências
const Post = require("../models/Post");
const sharp = require("sharp");
const path = require('path');
const fs = require('fs');

module.exports = {
    // Função que faz retorna a lista dos posts criados
    async index(req, res) {
        const posts = await Post.find().sort('-createdAt'); // Retorna os posts por ordem de ultima criação

        return res.json(posts);
    },

    // Função que armazena os posts
    async store(req, res) {
        // Atribuição dos valores dos campos recebidos às variáveis
        const { author, place, description, hashtags } = req.body;
        const { filename: image } = req.file;

        // Transforma qualquer imagem recebida no formato .jpg
        const [name] = image.split('.');
        const fileName = `${name}.jpg`;

        // Sharp para alterar as dimensões da imagem
        await sharp(req.file.path)
        .resize(500)
        .jpeg({quality: 70})
        .toFile(
            path.resolve(req.file.destination, 'resized', fileName) // Move a imagem com as dimensões alteradas para a pas uploads/resized
        )
        
        // Remoce a imagem original
        fs.unlinkSync(req.file.path);
            
        // Faz o armazenamento do post
        const post = await Post.create({
            author,
            place,
            description,
            hashtags,
            image: fileName
        });

        // Emite em tempo real para todas as urls
        req.io.emit('post', post);

        // retorna os dados criados
        return res.json(post);
    }
}