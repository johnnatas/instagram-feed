const Post = require("../models/Post");

module.exports = {

    // Função que armazena os likes em um determinado post
    async store(req, res) {
        const post = await Post.findById(req.params.id); // Encontra o post via ID enviado como parâmetro

        post.likes += 1; // Adiciona mais um like ao post

        await post.save(); // Salva o like

        req.io.emit('like', post); // Emite a atualização para todas as urls

        return res.json(post);
    }
}