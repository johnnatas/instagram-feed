const mongoose = require("mongoose");

// Criação da tabela de post na base de dados
const PostSchema = new mongoose.Schema({
    author: String,
    place: String,
    description: String,
    hashtags: String,
    image: String,
    likes: {
        type: Number,
        default: 0
    }
},{
    timestamps: true // Cria automaticamente o updatedAt e createdAt
});

module.exports = mongoose.model("Post", PostSchema);