// Importaçôes das dependências
const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const cors = require('cors');

const app = express();

// referenciando o io para http e também websocket
const server = require('http').Server(app);
const io = require('socket.io')(server);

// Conexão com o Banco de Dados MongoDB
mongoose.connect("mongodb+srv://admin:admin@cluster0-qpplu.mongodb.net/test?retryWrites=true&w=majority", {
    useNewUrlParser: true
});

// Middleware criadp para referenciar o io globalmente e poder passá-lo para as demais áreas da aplicação
app.use((req, res, next) => {
    req.io = io;

    next(); // Permite que a interceptação do middleware não pare aqui
});

// Permite que qualquer aplicação front possa utilizar as rotas
app.use(cors());

// Rota genérica criada para facilitar o acesso ao caminho das imagens armazenadas
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads', 'resized')));

// Usa as rotas definidas no arquivo routes.js
app.use(require("./routes"));

server.listen(3333);