// Importaçôes das dependências
const express = require("express");
const multer = require('multer');

// Importaçôes dos arquivos
const uploadConfig = require('./config/upload');
const PostController = require("./controllers/PostController");
const LikeController = require("./controllers/LikeController");

const routes = new express.Router();

// Multer permite manipular informações enviadas pela estrutura Multipart Form
const upload = multer(uploadConfig);

// Rotas
routes.get("/posts", PostController.index); // Retorna uma lista de posts criados
routes.post("/posts", upload.single('image'), PostController.store); // Cria um post
routes.post("/posts/:id/like", LikeController.store); // Adiciona um 'like' em um determinado post

module.exports = routes;