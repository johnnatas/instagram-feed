import React from 'react';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo.png';
import camera from '../../assets/camera.png';

import './header.css';

export default function Header() {
  return (
    <header id="main-header">
      <div className="header-content">
        <Link to="/new">
          <img src={camera} alt="Enviar publicação" />
        </Link>
        <Link to="/">
          <img src={logo} alt="InstaFeed" />
        </Link>
      </div>
    </header>
  );
}
