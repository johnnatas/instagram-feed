# Instagram Feed

Projeto de um clone do feed do Instagram. O objetivo principal é utilizar uma linguagem (Javascript) para fazer aplicações back-end, front-end e mobile. As tecnologias que serão utilizadas são: Node.js, React.js e React Native.

## **API**

- API criada está na pasta backend do projeto
- É necessário ter Node > 9
- Para instalar as dependências: *yarn install* ou *npm i*
- Para executar a API na porta 3333: *yarn dev* ou *npm dev*

### Rotas

- Para listar os posts criados: (via get) url + "/posts"
- Para criar os posts: (via post) url + "/posts"
- Para adicionar like a um determinado post: (via post) url + "/posts/" + id + "like"